const Deals = require('../../db/Deals');

module.exports = {
  getDeals(data) {
    return new Promise((resolve, reject) => {
      Deals
        .find(
          {
            "name": data.query,
          },
          null,
          {
            limit: data.limit,
            page: data.page,
          }
        )
        .then((result)=>{
          resolve(result);
        })
        .catch((error)=>{
          reject(error);
        })
    });
  },

  getSuggestion(data) {
    return new Promise((resolve, reject) => {
      Deals
        .find({ "name": data.query,},'name')
        .then((result)=>{
          let sortedResult =  this.getAutocompleteList(result, data.query);
          resolve(sortedResult);
        })
        .catch((error)=>{
          reject(error);
        })
    });
  },

  getAutocompleteList(result, query) {
    let sortedArr = [];
    result.entries.forEach((item)=>{
      let regEx = new RegExp('^'+query, 'i');
      if(regEx.test(item.name)) {
        sortedArr.push(item.name);
      }
    });
    return sortedArr;
  },
};