const db = [
  {
    "id" : "deal0",
    "name" : "Sushi",
    "discounted_price" : "30.00",
    "price" : "15.00",
    "outlet": {
      "id": "outlet2"
    }
  },
  {
    "id" : "deal1",
    "name" : "Sushi",
    "discounted_price" : "30.00",
    "price" : "15.00",
    "outlet": {
      "id": "outlet2"
    }
  },
  {
    "id" : "deal2",
    "name" : "Sushi",
    "discounted_price" : "30.00",
    "price" : "15.00",
    "outlet": {
      "id": "outlet2"
    }
  },
  {
    "id" : "deal3",
    "name" : "Sushi",
    "discounted_price" : "30.00",
    "price" : "15.00",
    "outlet": {
      "id": "outlet2"
    }
  },
  {
    "id" : "deal4",
    "name" : "Sushi",
    "discounted_price" : "30.00",
    "price" : "15.00",
    "outlet": {
      "id": "outlet2"
    }
  },
  {
    "id" : "deal5",
    "name" : "Sushi",
    "discounted_price" : "30.00",
    "price" : "15.00",
    "outlet": {
      "id": "outlet2"
    }
  },
  {
    "id" : "deal6",
    "name" : "Sushi",
    "discounted_price" : "30.00",
    "price" : "15.00",
    "outlet": {
      "id": "outlet2"
    }
  },
]

const result = db.slice(3, 6);

console.log(result);
// const _ = require('underscore');

// const result = _.pick({name: 'moe', age: 50, userid: 'moe1'}, ['name', 'age']);
// console.log(result);

// // var users = [
// //   { 'user': 'barney',  'active': false },
// //   { 'user': 'fred',    'active': false },
// //   { 'user': 'pebbles', 'active': true }
// // ];

// // const result = _.where(users, {active: false});
// // console.log(result);