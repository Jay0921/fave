# Seach API

### Installation
This project requires Node.js v6.4.0+ to run.
```sh
$ git clone git@gitlab.com:Jay0921/fave.git
$ npm install
$ node app
```

### Getting Started
By Default this porject is running on port 3000, you can change it in the .env file.
Below is the example of search api:
```sh
http://localhost:3000/api/v1/search?query=buffet&page=1&limit=2
```

### Options
| Name | Explanation |
| ------ | ------ |
| query | The name of the deal |
| page | Which page of result |
| limit | How many deals can be show at once |

### FRONTEND
You can access the Front-End page with this url
http://localhost:3000/

Example of keyword to search: steamboat, burger, korean, japanese. Please use these keyword for the search function as the dummy data is consist of these 4 type.

Search result limit can be change on src/config.js.


### TESTING
You can run the testing by the command below
```sh
npm test
```
For now, it's a very simple test. It checks the availability of all the api route.