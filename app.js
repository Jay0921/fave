require('dotenv').config();
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');

const apiV1 = require('./routes/api/v1');

const app = express();

app.use('/', express.static(path.join(__dirname, 'public')))

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use('/api', (req, res, next)=>{
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});
app.use('/api/v1', apiV1);

app.listen(process.env.PORT, () => {
  console.log(`Connected to Port ${process.env.PORT}`);
});

module.exports = app.listen(process.env.PORT, 'localhost');