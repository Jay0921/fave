const express = require('express');
const Search = require('../../../app/Domain/search');

const router = express.Router();

router.get('/search', (req, res, next) => {
  Search
    .getDeals(req.query)
    .then((deals)=>{
      res.status(200).send(deals);
    })
    .catch((error)=>{
      res.status(422).send(deals);
    });
});

router.get('/autocomplete', (req, res, next) => {
  Search
    .getSuggestion(req.query)
    .then((deals)=>{
      res.status(200).send(deals);
    })
    .catch((error)=>{
      res.status(422).send(error);
    });
});

module.exports = router;