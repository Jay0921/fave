const express = require('express');
const fs = require('fs');

const router = express.Router();

fs.readdirSync(__dirname).forEach((route) => {
  if (route !== 'index.js') {
    router.use('/', require(`${__dirname}/${route}`));
  }
});

module.exports = router;