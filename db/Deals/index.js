const _ = require('underscore');
const Deals = require('./data');

function insertIndex(result) {
  result.forEach((item, i)=> {
    i++;
    item.index = i;
  });
  return result;
}

function search(arr, query) {
  let searchResult = []

  arr.forEach((item, i)=>{
    let regEx = new RegExp(query.name, 'gi');
    if(regEx.test(item.name)) {
      searchResult.push(item);
    }
  });
  return searchResult;
}



function selectedField(result, fields) {
  let tempResult = [];
  let fieldsArr = fields.split(" ");
  _.each(result, (item)=>{
    let sortedFieldResult = _.pick(item, fieldsArr);
    tempResult.push(sortedFieldResult);
  });
  result = tempResult;
  return result;
}

function sortByPage(result, option) {
  totalPage = Math.ceil(result.length / option.limit);
  let start = option.page === "1" ? "0" : (option.limit * (option.page - 1));
  start = Number(option.page) > totalPage ? 0 : start;
  let end = start + Number(option.limit);
  result = result.slice(start, end);
  return {
    totalPage,
    result
  }
}

module.exports = {
  find(query, fields, option) {
    return new Promise((resolve, reject) => {
      try {
        let result = search(Deals, query);
        
        
        let total_entries = result.length;
        let totalPage;

        result = insertIndex(result);

        if (fields) {
          result = selectedField(result, fields);
        }

        if (option) {
          let sortedResult = sortByPage(result, option);
          totalPage = sortedResult.totalPage;
          result = sortedResult.result;
        }

        resolve({
          entries: result,
          page: {
            total_entries: total_entries,
            total_pages: totalPage
          }
        });
      } catch (error) {
        reject(error);
      }
    });
  },
}