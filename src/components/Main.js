import React from 'react';
import SearchBar from './SearchBar';
import Result from './Result';
import Pagination from './Pagination';

export default class Main extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return(
      <div className="container">
          <SearchBar/>
          <Result/>
          <Pagination/>
      </div>
    );
  }
}
