import React from 'react';
import { connect } from 'react-redux';

class Result extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  generateReult() {
    let result;
    if(this.props.search.result) {
      result = this.props.search.result.entries.map((item) => 
        <tr key={item.id}>
          <td>{item.index}</td>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>RM {item.price}</td>
          <td>RM {item.discounted_price}</td>
          <td>{item.outlet.id}</td>
        </tr>
      );
    }
    return result;
  }

  render() {
    return (
      <div>
        <h1>Search Result</h1>
        <div className="ResultTable">
          <table className="table table-dark">
            <thead>
              <tr>
                <th>INDEX</th>
                <th>ID</th>
                <th>NAME</th>
                <th>PRICE</th>
                <th>DISCOUNTED PRICE</th>
                <th>OUTLET ID</th>
              </tr>
            </thead>
            <tbody>
              {this.generateReult()}
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return state;
}

export default connect (mapStateToProps)(Result)