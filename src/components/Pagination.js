import React from 'react';

import { connect } from 'react-redux';
import { updateSearch } from '../action/searchAction';

class Pagination extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  onChange(event) {
    this.props.onUpdateSearch({
      query: this.props.search.query,
      page: event.target.value
    });
  }

  generatePagination() {
    let totalPage;
    let pagination = [];
    if(this.props.search.result) {
      totalPage = this.props.search.result.page.total_pages;
    }
    for(var i = 1; i <= totalPage; i++) {
      pagination.push(
        <li className="page-item" key={i}>
          <input className="page-link" key={i} type="button" value={i} onClick={this.onChange.bind(this)}/>
        </li>
        // 
      )
    }
    return pagination;
  }

  render() {
    return(
      <nav>
        <ul className="pagination justify-content-center">
          {this.generatePagination()}
        </ul>
      </nav>
    );
  }
}

const mapStateToProps = state => {
  return state;
}

const mapActionsToProps = {
  onUpdateSearch: updateSearch
}

export default connect (mapStateToProps, mapActionsToProps)(Pagination)