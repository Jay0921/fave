import React from 'react';
import config from '../config'
import axios from 'axios';
import { connect } from 'react-redux';
import { updateSearch } from '../action/searchAction';

class SearchBar extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      autoComplete: false
    };
  }

  handleChange(event) {

    if(event.target.value !== '') {
      axios.get(`${config.endpoint}/api/v1/autocomplete?query=${event.target.value}`)
        .then((response)=>{
          this.setState({
            autoComplete: true,
            result: response.data
          });
        })
    }

    this.setState({
      value: event.target.value
    });
  }

  onUpdateSearch() {
    this.props.onUpdateSearch({
      query: this.state.value,
      page: 1
    });
  }

  handleClick(event) {
    this.setState({
      value: event.target.value,
      autoComplete: false,
    });
    this.props.onUpdateSearch({
      query: event.target.value,
      page: 1
    });
  }

  handleBlur() {
    let self = this;
    setTimeout(function(){
      self.setState({
        autoComplete: false,
      });
    }, 200);
  }

  generateSuggestion() {
    let item;
    if (this.state.result) {
      item = this.state.result.map((item)=>
      <input className="AutoComplete__item" type="button" key={item} value={item} onClick={this.handleClick.bind(this)}/>
      );
    } else {
      item = [];
    }
    return item;
  }

  render() {
    return(
      <div className="row">
        <div className="col-sm-6">
          <div className="form-group InputSearch">
            <label>Search</label>
            <input className="form-control" type="text" onChange={this.handleChange.bind(this)} placeholder="Deal Name" onBlur={this.handleBlur.bind(this)}/>
            <div className="AutoComplete"> 
              {this.state.autoComplete ? this.generateSuggestion() : null }
            </div>
          </div>
        </div>
        <div className="col-sm-6">
          <label>&nbsp;</label>
          <button className="btn btn-info btn-block" onClick={this.onUpdateSearch.bind(this)}>Search</button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return state;
}

const mapActionsToProps = {
  onUpdateSearch: updateSearch
}

export default connect (mapStateToProps, mapActionsToProps)(SearchBar)