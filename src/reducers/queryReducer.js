import { UPDATE_QUERY } from '../action/queryAction';

export default function updateQuery(state = '', {type, payload}) {
  switch (type) {
    case UPDATE_QUERY:
        return payload.query;
      break;
    default:
      return 'State'
      break;
  }
}