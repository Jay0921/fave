import searchReducer from './searchReducer';

module.exports = {
  search: searchReducer
}