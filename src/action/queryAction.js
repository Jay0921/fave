export const  UPDATE_QUERY = 'search:updateQuery';

export function updateQuery(query) {
  return {
    type: UPDATE_QUERY,
    payload: {
      query: query
    }
  }
}