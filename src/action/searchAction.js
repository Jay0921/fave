import config from '../config'
import axios from 'axios';
export const  RECEIVE_SEARCH = 'search:receiveSearch';


export function receiveSearch(query, result) {
  return {
    type: RECEIVE_SEARCH,
    payload: {
      query: query,
      result: result.data
    }
  }
}

export function updateSearch(option) {
  return function(dispatch) {
    return axios.get(`${config.endpoint}/api/v1/search?query=${option.query}&page=${option.page}&limit=${config.limit}`)
      .then((data)=>{
        dispatch(receiveSearch(option.query, data));
      })
  }
}