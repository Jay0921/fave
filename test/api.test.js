const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');

const assert = chai.assert;
const expect = chai.expect;
chai.use(chaiHttp);

checknormal = (err, res) => {
  expect(err).to.be.null;
  expect(res).to.have.status(200);
};


describe('API Route', ()=>{

  describe('/api/v1/search', () => {
    it('Should return you an huge object', () => {
      chai.request(server)
        .get('/api/v1/search')
        .end((err, res)=>{
          checknormal(err, res);
          expect(res.body).to.not.be.empty;
        });
    });
  });

  describe('/api/v1/autocomplete', () => {
    it('Should return you empty array if you didn\'t put any search query', () => {
      chai.request(server)
        .get('/api/v1/autocomplete')
        .end((err, res)=>{
          checknormal(err, res);
        });
    });
  });

});